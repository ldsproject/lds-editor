﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class MarkerDetector : MonoBehaviour, ITrackableEventHandler
{

    protected TrackableBehaviour mTrackableBehaviour;

    public bool renderingIsEnabled = true;
    public bool canvasIsEnabled = true;


    public delegate void TrackingLost();
    public TrackingLost trackingLost;
    public delegate void TrackingFound(string markerName);
    public TrackingFound trackingFound;


    protected virtual void Start()
    {
        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
            mTrackableBehaviour.RegisterTrackableEventHandler(this);

    }

    protected virtual void OnDestroy()
    {
        if (mTrackableBehaviour)
            mTrackableBehaviour.UnregisterTrackableEventHandler(this);
    }


    /// <summary>
    ///     Implementation of the ITrackableEventHandler function called when the
    ///     tracking state changes.
    /// </summary>
    public void OnTrackableStateChanged(
        TrackableBehaviour.Status previousStatus,
        TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            OnTrackingFound();
        }
        else if (previousStatus == TrackableBehaviour.Status.TRACKED &&
                 newStatus == TrackableBehaviour.Status.NO_POSE)
        {
            OnTrackingLost();
        }
        else
        {
            // For combo of previousStatus=UNKNOWN + newStatus=UNKNOWN|NOT_FOUND
            // Vuforia is starting, but tracking has not been lost or found yet
            // Call OnTrackingLost() to hide the augmentations
            OnTrackingLost();
        }
    }

    protected virtual void OnTrackingFound()
    {
        if (renderingIsEnabled)
        {
            SetRendering(true);
        }

        if (canvasIsEnabled)
        {
            SetCanvas(true);
        }


        if (trackingFound != null)
        {
            trackingFound(mTrackableBehaviour.TrackableName);
        }

        Debug.Log("tracking found! name of the marker: " + mTrackableBehaviour.TrackableName);
    }

    protected virtual void OnTrackingLost()
    {
        if (renderingIsEnabled)
        {
            SetRendering(false);
        }

        if (canvasIsEnabled)
        {
            SetCanvas(false);
        }

        if (trackingLost != null)
        {
            trackingLost();
        }

        Debug.Log("tracking lost!");
    }


    void SetRendering(bool isEnabled)
    {
        var rendererComponents = GetComponentsInChildren<Renderer>(true);
        foreach (var component in rendererComponents)
        {
            component.enabled = isEnabled;
        }
    }

    void SetCanvas(bool isEnabled)
    {
        var canvasComponents = GetComponentsInChildren<Canvas>(true);
        foreach (var component in canvasComponents)
            component.enabled = isEnabled;

    }
}
